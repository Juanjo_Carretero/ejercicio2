package Ejercicio2.app;

public class Ejercicio2app {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARACI�N DE VARIABLES
		int n = 5;
		double a = 4.56;
		char c = 'a';
		int valnumchar = (int) c;
		
		//VALOR DE LA N
		System.out.println("VALOR DE LA VARIABLE N: " + n);
		
		//VALOR DE LA A
		System.out.println("VALOR DE LA VARIABLE A: " + a);
		
		//VALOR DE LA C
		System.out.println("VALOR DE LA VARIABLE C: " + c);
		
		//VALOR DE LA N+A
		System.out.println("5 + 4.56 = " + (n+a));
		
		//VALOR DE LA A-N
		System.out.println("4.56 - 5 = " + (a-n));
		
		//VALOR NUM�RICO DE EL CAR�CTER A
		System.out.println("VALOR NUM�RICO DEL CAR�CTER A: " + valnumchar);
	}

}
